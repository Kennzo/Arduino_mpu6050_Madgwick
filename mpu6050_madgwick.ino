#include <Wire.h>
#include "MadgwickAHRS.h"
#include "MPU6050.h"

Madgwick filter;
MPU6050 mpu6050;

unsigned long millisPerReading, millisPrevious;

void setup() {
  Serial.begin(9600);
  Wire.begin();

  mpu6050.init();
  filter.begin(25);

  millisPerReading = 1000 / 25;
  millisPrevious = millis();
}

void loop() {
  unsigned long millisNow = millis();
  float imuData[6];

  if(millisNow - millisPrevious >= millisPerReading) {
    mpu6050.getData(imuData);
    
    filter.updateIMU(imuData[0], imuData[1], imuData[2], imuData[3], imuData[4], imuData[5]);
    
    float roll = filter.getRoll();
    float pitch = filter.getPitch();
    float heading = filter.getYaw();
  
    Serial.print("Orientation: ");
    Serial.print(heading);
    Serial.print(" ");
    Serial.print(pitch);
    Serial.print(" ");
    Serial.println(roll);

    millisPrevious += millisPerReading;
  }
}
